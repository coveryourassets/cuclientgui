/****************************************************************************
** Meta object code from reading C++ file 'patientwindow.h'
**
** Created: Fri Nov 23 21:57:33 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "patientwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'patientwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PatientWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,
      37,   14,   14,   14, 0x0a,
      60,   14,   14,   14, 0x08,
      87,   14,   14,   14, 0x08,
     118,  112,   14,   14, 0x08,
     160,   14,   14,   14, 0x08,
     185,   14,   14,   14, 0x08,
     204,   14,   14,   14, 0x08,
     222,   14,   14,   14, 0x08,
     241,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PatientWindow[] = {
    "PatientWindow\0\0populatePatientForm()\0"
    "populatePatientTable()\0"
    "on_createPatient_clicked()\0"
    "on_savePatient_clicked()\0index\0"
    "on_tableWidget_doubleClicked(QModelIndex)\0"
    "on_getConsults_clicked()\0on_clear_clicked()\0"
    "on_exit_clicked()\0clearPatientForm()\0"
    "update()\0"
};

void PatientWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PatientWindow *_t = static_cast<PatientWindow *>(_o);
        switch (_id) {
        case 0: _t->populatePatientForm(); break;
        case 1: _t->populatePatientTable(); break;
        case 2: _t->on_createPatient_clicked(); break;
        case 3: _t->on_savePatient_clicked(); break;
        case 4: _t->on_tableWidget_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->on_getConsults_clicked(); break;
        case 6: _t->on_clear_clicked(); break;
        case 7: _t->on_exit_clicked(); break;
        case 8: _t->clearPatientForm(); break;
        case 9: _t->update(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PatientWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PatientWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_PatientWindow,
      qt_meta_data_PatientWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PatientWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PatientWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PatientWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PatientWindow))
        return static_cast<void*>(const_cast< PatientWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int PatientWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
