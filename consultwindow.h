#ifndef CONSULTWINDOW_H
#define CONSULTWINDOW_H

#include <QMainWindow>
#include "maincontroller.h"

class MainController;

namespace Ui {
class ConsultWindow;
}

class ConsultWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ConsultWindow(MainController * control, QWidget *parent = 0);
    ~ConsultWindow();

    void initializeWithNewPatient();

    void setCurrentConsult(Consultation *consult);
    Consultation **getCurrentConsult();

    
private slots:
    void on_save_clicked();

    void on_back_clicked();

    void on_tableWidget_doubleClicked(const QModelIndex &index);


    void on_clear_clicked();

    void on_newfollowup_clicked();

    void on_getfollowups_clicked();

private:
    void populateConsult();
    void populateConsultTable();
    void clear();

    Ui::ConsultWindow *ui;
    MainController *control;
    QList<Consultation> *consults;
    QList<Followup> *followups;
    Consultation **currentConsult;
    Patient **currentPatient;
    cuStore* storage;
    User * user;
};

#endif // CONSULTWINDOW_H

