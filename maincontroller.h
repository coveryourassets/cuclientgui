#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include "storage/custore.h"
#include "mainwindow.h"
#include "patientwindow.h"
#include "logindialog.h"
#include "consultwindow.h"
#include "followupwindow.h"
#include "followupwidget.h"

class LoginDialog;
class MainWindow;
class PatientSearchDialog;
class PatientWindow;
class ConsultWindow;
class FollowupWindow;
class FollowupWidget;

class MainController
{
public:
    MainController();
    ~MainController();

    //setters
    void setPatients(QList<Patient> *patients);
    void setConsults(QList<Consultation> *consults);
    void setCurrentPatient(Patient* patient);
    void setCurrentConsult(Consultation* consult);

    //getters
    QList<Patient> *getPatients();
    QList<Consultation> *getConsults();
    QList<Followup> *getFollowups();
    Patient ** getCurrentPatient();
    Consultation ** getCurrentConsult();
    Followup ** getCurrentFollowup();
    cuStore * getStorage();
    User * getUser();

    //etc
    void addPatient(Patient* pat);

    //window callbacks
    void loginSuccess();
    void newPatient();
    void newConsult();
    void exitPatientWindow(); 
    void exitConsultWindow();
    void openPatientWindow();
    void openConsultWindow();
    void openFollowupWindow();

private:
    cuStore * storage;
    User * user;
    LoginDialog * login;
    MainWindow * mainWindow;
    PatientWindow * patwindow;
    PatientSearchDialog *psearchdialog;
    ConsultWindow *consultwindow;
    FollowupWindow *followupwindow;
    FollowupWidget *followupwidget;

    Patient * currentPatient;
    QList<Patient> *patients;
    QList<Consultation> *consults;
    Consultation * currentConsult;
    QList<Followup> *followups;
    Followup * currentFollowup;

};

#endif // MAINCONTROLLER_H
