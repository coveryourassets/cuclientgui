/********************************************************************************
** Form generated from reading UI file 'patientwindow.ui'
**
** Created: Fri Nov 23 15:50:54 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PATIENTWINDOW_H
#define UI_PATIENTWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateEdit>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PatientWindow
{
public:
    QAction *actionCreate;
    QWidget *centralWidget;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *fname;
    QLabel *label_2;
    QLineEdit *lname;
    QLabel *label_5;
    QLineEdit *healthcard;
    QLabel *label_6;
    QLineEdit *healthinsurance;
    QWidget *formLayoutWidget_2;
    QFormLayout *addressLayout;
    QLabel *streetlabel;
    QLineEdit *street;
    QLabel *citylabel;
    QLineEdit *city;
    QLabel *provincelabel;
    QLabel *label_3;
    QLineEdit *postalcode;
    QComboBox *province;
    QLabel *label_4;
    QWidget *formLayoutWidget_3;
    QFormLayout *contactlayout;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *homephone;
    QLineEdit *cellphone;
    QLineEdit *workphone;
    QLineEdit *ext;
    QLineEdit *primary;
    QWidget *formLayoutWidget_4;
    QFormLayout *formLayout_2;
    QLabel *label_12;
    QDateEdit *dateofbirth;
    QLabel *label_13;
    QTextEdit *comments;
    QPushButton *savePatient;
    QPushButton *clear;
    QPushButton *exit;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QTableWidget *tableWidget;
    QLabel *label_14;
    QPushButton *getConsults;
    QMenuBar *menuBar;
    QMenu *menuPatient_Form;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PatientWindow)
    {
        if (PatientWindow->objectName().isEmpty())
            PatientWindow->setObjectName(QString::fromUtf8("PatientWindow"));
        PatientWindow->resize(662, 615);
        actionCreate = new QAction(PatientWindow);
        actionCreate->setObjectName(QString::fromUtf8("actionCreate"));
        centralWidget = new QWidget(PatientWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        formLayoutWidget = new QWidget(centralWidget);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 40, 331, 161));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        fname = new QLineEdit(formLayoutWidget);
        fname->setObjectName(QString::fromUtf8("fname"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fname);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        lname = new QLineEdit(formLayoutWidget);
        lname->setObjectName(QString::fromUtf8("lname"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lname);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        healthcard = new QLineEdit(formLayoutWidget);
        healthcard->setObjectName(QString::fromUtf8("healthcard"));

        formLayout->setWidget(2, QFormLayout::FieldRole, healthcard);

        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_6);

        healthinsurance = new QLineEdit(formLayoutWidget);
        healthinsurance->setObjectName(QString::fromUtf8("healthinsurance"));

        formLayout->setWidget(3, QFormLayout::FieldRole, healthinsurance);

        formLayoutWidget_2 = new QWidget(centralWidget);
        formLayoutWidget_2->setObjectName(QString::fromUtf8("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(340, 40, 301, 141));
        addressLayout = new QFormLayout(formLayoutWidget_2);
        addressLayout->setSpacing(6);
        addressLayout->setContentsMargins(11, 11, 11, 11);
        addressLayout->setObjectName(QString::fromUtf8("addressLayout"));
        addressLayout->setContentsMargins(0, 0, 0, 0);
        streetlabel = new QLabel(formLayoutWidget_2);
        streetlabel->setObjectName(QString::fromUtf8("streetlabel"));

        addressLayout->setWidget(0, QFormLayout::LabelRole, streetlabel);

        street = new QLineEdit(formLayoutWidget_2);
        street->setObjectName(QString::fromUtf8("street"));

        addressLayout->setWidget(0, QFormLayout::FieldRole, street);

        citylabel = new QLabel(formLayoutWidget_2);
        citylabel->setObjectName(QString::fromUtf8("citylabel"));

        addressLayout->setWidget(1, QFormLayout::LabelRole, citylabel);

        city = new QLineEdit(formLayoutWidget_2);
        city->setObjectName(QString::fromUtf8("city"));

        addressLayout->setWidget(1, QFormLayout::FieldRole, city);

        provincelabel = new QLabel(formLayoutWidget_2);
        provincelabel->setObjectName(QString::fromUtf8("provincelabel"));

        addressLayout->setWidget(2, QFormLayout::LabelRole, provincelabel);

        label_3 = new QLabel(formLayoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        addressLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        postalcode = new QLineEdit(formLayoutWidget_2);
        postalcode->setObjectName(QString::fromUtf8("postalcode"));

        addressLayout->setWidget(3, QFormLayout::FieldRole, postalcode);

        province = new QComboBox(formLayoutWidget_2);
        province->setObjectName(QString::fromUtf8("province"));

        addressLayout->setWidget(2, QFormLayout::FieldRole, province);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 10, 64, 17));
        formLayoutWidget_3 = new QWidget(centralWidget);
        formLayoutWidget_3->setObjectName(QString::fromUtf8("formLayoutWidget_3"));
        formLayoutWidget_3->setGeometry(QRect(340, 190, 301, 161));
        contactlayout = new QFormLayout(formLayoutWidget_3);
        contactlayout->setSpacing(6);
        contactlayout->setContentsMargins(11, 11, 11, 11);
        contactlayout->setObjectName(QString::fromUtf8("contactlayout"));
        contactlayout->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(formLayoutWidget_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        contactlayout->setWidget(0, QFormLayout::LabelRole, label_7);

        label_8 = new QLabel(formLayoutWidget_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        contactlayout->setWidget(1, QFormLayout::LabelRole, label_8);

        label_9 = new QLabel(formLayoutWidget_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        contactlayout->setWidget(2, QFormLayout::LabelRole, label_9);

        label_10 = new QLabel(formLayoutWidget_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        contactlayout->setWidget(3, QFormLayout::LabelRole, label_10);

        label_11 = new QLabel(formLayoutWidget_3);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        contactlayout->setWidget(4, QFormLayout::LabelRole, label_11);

        homephone = new QLineEdit(formLayoutWidget_3);
        homephone->setObjectName(QString::fromUtf8("homephone"));

        contactlayout->setWidget(0, QFormLayout::FieldRole, homephone);

        cellphone = new QLineEdit(formLayoutWidget_3);
        cellphone->setObjectName(QString::fromUtf8("cellphone"));

        contactlayout->setWidget(1, QFormLayout::FieldRole, cellphone);

        workphone = new QLineEdit(formLayoutWidget_3);
        workphone->setObjectName(QString::fromUtf8("workphone"));

        contactlayout->setWidget(2, QFormLayout::FieldRole, workphone);

        ext = new QLineEdit(formLayoutWidget_3);
        ext->setObjectName(QString::fromUtf8("ext"));

        contactlayout->setWidget(3, QFormLayout::FieldRole, ext);

        primary = new QLineEdit(formLayoutWidget_3);
        primary->setObjectName(QString::fromUtf8("primary"));

        contactlayout->setWidget(4, QFormLayout::FieldRole, primary);

        formLayoutWidget_4 = new QWidget(centralWidget);
        formLayoutWidget_4->setObjectName(QString::fromUtf8("formLayoutWidget_4"));
        formLayoutWidget_4->setGeometry(QRect(0, 190, 331, 161));
        formLayout_2 = new QFormLayout(formLayoutWidget_4);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_12 = new QLabel(formLayoutWidget_4);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_12);

        dateofbirth = new QDateEdit(formLayoutWidget_4);
        dateofbirth->setObjectName(QString::fromUtf8("dateofbirth"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, dateofbirth);

        label_13 = new QLabel(formLayoutWidget_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_13);

        comments = new QTextEdit(formLayoutWidget_4);
        comments->setObjectName(QString::fromUtf8("comments"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, comments);

        savePatient = new QPushButton(centralWidget);
        savePatient->setObjectName(QString::fromUtf8("savePatient"));
        savePatient->setGeometry(QRect(450, 0, 94, 27));
        clear = new QPushButton(centralWidget);
        clear->setObjectName(QString::fromUtf8("clear"));
        clear->setGeometry(QRect(350, 0, 94, 27));
        exit = new QPushButton(centralWidget);
        exit->setObjectName(QString::fromUtf8("exit"));
        exit->setGeometry(QRect(550, 0, 94, 27));
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(0, 390, 661, 181));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 659, 179));
        tableWidget = new QTableWidget(scrollAreaWidgetContents);
        if (tableWidget->columnCount() < 5)
            tableWidget->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 641, 192));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->horizontalHeader()->setDefaultSectionSize(120);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setVisible(false);
        scrollArea->setWidget(scrollAreaWidgetContents);
        label_14 = new QLabel(centralWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(0, 370, 111, 17));
        getConsults = new QPushButton(centralWidget);
        getConsults->setObjectName(QString::fromUtf8("getConsults"));
        getConsults->setGeometry(QRect(200, 0, 141, 27));
        PatientWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PatientWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 662, 25));
        menuPatient_Form = new QMenu(menuBar);
        menuPatient_Form->setObjectName(QString::fromUtf8("menuPatient_Form"));
        PatientWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(PatientWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        PatientWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(PatientWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        PatientWindow->setStatusBar(statusBar);

        menuBar->addAction(menuPatient_Form->menuAction());
        menuPatient_Form->addAction(actionCreate);

        retranslateUi(PatientWindow);

        QMetaObject::connectSlotsByName(PatientWindow);
    } // setupUi

    void retranslateUi(QMainWindow *PatientWindow)
    {
        PatientWindow->setWindowTitle(QApplication::translate("PatientWindow", "PatientWindow", 0, QApplication::UnicodeUTF8));
        actionCreate->setText(QApplication::translate("PatientWindow", "create", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("PatientWindow", "First Name", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("PatientWindow", "Last Name", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("PatientWindow", "HealthCard", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("PatientWindow", "Health Insurance", 0, QApplication::UnicodeUTF8));
        streetlabel->setText(QApplication::translate("PatientWindow", "Street", 0, QApplication::UnicodeUTF8));
        citylabel->setText(QApplication::translate("PatientWindow", "City", 0, QApplication::UnicodeUTF8));
        provincelabel->setText(QApplication::translate("PatientWindow", "Province", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("PatientWindow", "Postal Code", 0, QApplication::UnicodeUTF8));
        province->clear();
        province->insertItems(0, QStringList()
         << QApplication::translate("PatientWindow", "Ontario", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("PatientWindow", "Quebec", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("PatientWindow", "Other", 0, QApplication::UnicodeUTF8)
        );
        label_4->setText(QApplication::translate("PatientWindow", "Patient", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("PatientWindow", "Home phone", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("PatientWindow", "Cell phone", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("PatientWindow", "Work phone", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("PatientWindow", "Ext", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("PatientWindow", "Primary Contact", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("PatientWindow", "Date of Birth", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("PatientWindow", "Comments", 0, QApplication::UnicodeUTF8));
        savePatient->setText(QApplication::translate("PatientWindow", "Save", 0, QApplication::UnicodeUTF8));
        clear->setText(QApplication::translate("PatientWindow", "Clear", 0, QApplication::UnicodeUTF8));
        exit->setText(QApplication::translate("PatientWindow", "Back", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("PatientWindow", "First Name", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("PatientWindow", "Last Name", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("PatientWindow", "HealthCard", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("PatientWindow", "Insurance", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("PatientWindow", "Primary Contact", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("PatientWindow", "Patient List", 0, QApplication::UnicodeUTF8));
        getConsults->setText(QApplication::translate("PatientWindow", "Get Consultations", 0, QApplication::UnicodeUTF8));
        menuPatient_Form->setTitle(QApplication::translate("PatientWindow", "Patient", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PatientWindow: public Ui_PatientWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PATIENTWINDOW_H
