#ifndef FOLLOWUPWINDOW_H
#define FOLLOWUPWINDOW_H

#include <QMainWindow>
#include "maincontroller.h"

class FollowupWidget;

namespace Ui {
class FollowupWindow;
}

class FollowupWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit FollowupWindow(MainController *control, QWidget *parent = 0);
    ~FollowupWindow();

    FollowupWidget *getFollowupWidget();
    
private:
    Ui::FollowupWindow *ui;
    MainController *control;
    FollowupWidget * fupwidget;
};

#endif // FOLLOWUPWINDOW_H
