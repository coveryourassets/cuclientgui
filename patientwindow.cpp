#include "patientwindow.h"
#include "ui_patientwindow.h"
#include <QtGui>

PatientWindow::PatientWindow(MainController *control, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PatientWindow)
{
    ui->setupUi(this);
    this->control = control;
    patients = control->getPatients();
    storage = control->getStorage();
    user = control->getUser();
    currentPatient = control->getCurrentPatient();
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
}

PatientWindow::~PatientWindow()
{
    delete ui;
}


void PatientWindow::on_createPatient_clicked()
{
    /*
    Patient( QString healthcardno
           , QString healthinsuranceno
           , QString firstName
           , QString lastName
           , Address &address
           , ContactInfo &contactinfo);
           */

}

void PatientWindow::on_savePatient_clicked()
{
    QString message;
    QString button;
    bool update = false;
    if (*currentPatient == 0){
        message.append("Save new patient?");
        button.append("Save");
    }else{
        message.append("Update patient?");
        button.append("Update");
        update = true;
    }
    if( QMessageBox::information( this, "Patient Save",
                                          message,
                                          button, "Cancel",
                                          0, 1 ) == 1) {


        //user pressed cancel
        qDebug("Cancelling");
        return;
    }
    Address add(ui->street->text(),QString(""),ui->city->text(),(qint32)ui->province->currentIndex(),ui->postalcode->text());
    ContactInfo info(ui->ext->text(),ui->cellphone->text(),ui->homephone->text(),ui->workphone->text(),ui->primary->text());
    if (update){
        QString healthcardo(ui->healthcard->text());
        QString healthinsurance(ui->healthinsurance->text());
        QString fname(ui->fname->text());
        QString lname(ui->lname->text());
        (*currentPatient)->setHealthcardNo(healthcardo);
        (*currentPatient)->setHealthInsuranceNo(healthinsurance);
        (*currentPatient)->setFirstName(fname);
        (*currentPatient)->setLastName(lname);
        (*currentPatient)->setAddress(add);
        (*currentPatient)->setContactInfo(info);
    }else{
        (*currentPatient) = new Patient(ui->healthcard->text(), ui->healthinsurance->text(), ui->fname->text(), ui->lname->text(),
                               add, info);
    }
    //if there is no currentPatient it is a save
    //if there is a currentPatient it is an update
    errorType t;
    if (update){
        t = storage->updatePatient(**currentPatient);
    }else{
        t = storage->saveNewPatient(**currentPatient);
    }
    message.clear();
    message.append(button);
    if (t==OK){
        message.append(" successful");
        if (!update){
            patients->push_back(**currentPatient);
        }
    }else{
        message.append(" failed");
        delete *currentPatient;
        (*currentPatient)=0;
    }
    populatePatientTable();
    QMessageBox::information( this, "Patient Save",
                                              message,
                                "OK",
                                              0);
}

void PatientWindow::on_tableWidget_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    (*currentPatient) = &(*patients)[row];
    populatePatientForm();
}


void PatientWindow::on_getConsults_clicked()
{
    if (*currentPatient==0){
        QMessageBox::information( this, "Attention",
                                  "No patient selected. Please select or create patient before viewing or creating "
                                  "consultations",
                                    "OK",0);
        return;
    }
    control->openConsultWindow();
}

void PatientWindow::clearPatientForm(){
    //clear address
    ui->street->clear();
    ui->city->clear();
    ui->postalcode->clear();

    //clear contact info
    ui->ext->clear();
    ui->cellphone->clear();
    ui->workphone->clear();
    ui->homephone->clear();
    ui->primary->clear();

    //clear patient
    ui->fname->clear();
    ui->lname->clear();
    ui->healthcard->clear();
    ui->healthinsurance->clear();
    ui->comments->clear();

    (*currentPatient)=0;
}

void PatientWindow::populatePatientForm(){

    if ((*currentPatient) == 0){
        return;
    }
    ui->fname->setText((*currentPatient)->getFirstName());
    ui->lname->setText((*currentPatient)->getLastName());
    ui->healthcard->setText((*currentPatient)->getHealthcardNo());
    ui->healthinsurance->setText((*currentPatient)->getHealthInsuranceNo());
    ui->comments->setText((*currentPatient)->getComments());

    const Address *add = &((*currentPatient)->getAddress());
    ui->street->setText(add->getAddressLineOne());
    ui->city->setText(add->getCity());
    ui->province->setCurrentIndex(add->getProvince());
    ui->postalcode->setText(add->getPostalCode());

    const ContactInfo *info = &((*currentPatient)->getContactInfo());
    ui->ext->setText(info->getExt());
    ui->cellphone->setText(info->getCellNo());
    ui->homephone->setText(info->getHomeNo());
    ui->workphone->setText(info->getOfficeNo());
    ui->primary->setText(info->getPrimaryNo());

}

void PatientWindow::update(){

}

void PatientWindow::on_clear_clicked()
{
    clearPatientForm();
    control->setCurrentPatient(0);
}

void PatientWindow::on_exit_clicked()
{
    control->exitPatientWindow();
}

void PatientWindow::populatePatientTable(){
    Patient *pat;
    QString num;
    qDebug("Populating patients: %d", patients->size());
    ui->tableWidget->setRowCount(patients->size());
    QTableWidgetItem *item;
    for (int i = 0; i < patients->size(); i ++){
        pat = &(*patients)[i];
        qDebug(pat->getLastName().toStdString().c_str());
        //Physician date reason diagnosis
       //QTableWidgetItem item(consult->)
        item = new QTableWidgetItem(pat->getFirstName());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tableWidget->setItem(i,0,item);
        item = new QTableWidgetItem(pat->getLastName());
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
         ui->tableWidget->setItem(i,1,item);
         item = new QTableWidgetItem(pat->getHealthcardNo());
         item->setFlags(item->flags() ^ Qt::ItemIsEditable);
         ui->tableWidget->setItem(i,2,item);
         item = new QTableWidgetItem(pat->getHealthInsuranceNo());
         item->setFlags(item->flags() ^ Qt::ItemIsEditable);
         ui->tableWidget->setItem(i,3,item);
         item = new QTableWidgetItem(pat->getContactInfo().getPrimaryNo());
         item->setFlags(item->flags() ^ Qt::ItemIsEditable);
         ui->tableWidget->setItem(i,4,item);

    }
}

