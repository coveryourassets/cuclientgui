/********************************************************************************
** Form generated from reading UI file 'followupwindow.ui'
**
** Created: Fri Nov 23 15:50:54 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FOLLOWUPWINDOW_H
#define UI_FOLLOWUPWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FollowupWindow
{
public:
    QWidget *centralwidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *FollowupWindow)
    {
        if (FollowupWindow->objectName().isEmpty())
            FollowupWindow->setObjectName(QString::fromUtf8("FollowupWindow"));
        FollowupWindow->resize(800, 600);
        centralwidget = new QWidget(FollowupWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        FollowupWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(FollowupWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        FollowupWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(FollowupWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        FollowupWindow->setStatusBar(statusbar);

        retranslateUi(FollowupWindow);

        QMetaObject::connectSlotsByName(FollowupWindow);
    } // setupUi

    void retranslateUi(QMainWindow *FollowupWindow)
    {
        FollowupWindow->setWindowTitle(QApplication::translate("FollowupWindow", "Follow-up WIndow", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FollowupWindow: public Ui_FollowupWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FOLLOWUPWINDOW_H
