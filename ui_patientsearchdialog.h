/********************************************************************************
** Form generated from reading UI file 'patientsearchdialog.ui'
**
** Created: Wed Nov 21 12:20:14 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PATIENTSEARCHDIALOG_H
#define UI_PATIENTSEARCHDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PatientSearchDialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *fname;
    QLineEdit *lname;
    QLabel *label_3;
    QLineEdit *hcard;
    QLabel *label_2;
    QLabel *label_4;
    QPushButton *searchfname;
    QPushButton *searchlname;
    QPushButton *searchhcard;
    QPushButton *searchfullname;
    QLabel *label_5;
    QPushButton *cancel;

    void setupUi(QDialog *PatientSearchDialog)
    {
        if (PatientSearchDialog->objectName().isEmpty())
            PatientSearchDialog->setObjectName(QString::fromUtf8("PatientSearchDialog"));
        PatientSearchDialog->resize(456, 351);
        formLayoutWidget = new QWidget(PatientSearchDialog);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 80, 411, 131));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        fname = new QLineEdit(formLayoutWidget);
        fname->setObjectName(QString::fromUtf8("fname"));

        formLayout->setWidget(0, QFormLayout::FieldRole, fname);

        lname = new QLineEdit(formLayoutWidget);
        lname->setObjectName(QString::fromUtf8("lname"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lname);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        hcard = new QLineEdit(formLayoutWidget);
        hcard->setObjectName(QString::fromUtf8("hcard"));

        formLayout->setWidget(2, QFormLayout::FieldRole, hcard);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_4 = new QLabel(PatientSearchDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(40, 30, 251, 41));
        QFont font;
        font.setFamily(QString::fromUtf8("Bitstream Charter"));
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);
        searchfname = new QPushButton(PatientSearchDialog);
        searchfname->setObjectName(QString::fromUtf8("searchfname"));
        searchfname->setGeometry(QRect(10, 260, 94, 27));
        searchlname = new QPushButton(PatientSearchDialog);
        searchlname->setObjectName(QString::fromUtf8("searchlname"));
        searchlname->setGeometry(QRect(120, 260, 94, 27));
        searchhcard = new QPushButton(PatientSearchDialog);
        searchhcard->setObjectName(QString::fromUtf8("searchhcard"));
        searchhcard->setGeometry(QRect(340, 260, 94, 27));
        searchfullname = new QPushButton(PatientSearchDialog);
        searchfullname->setObjectName(QString::fromUtf8("searchfullname"));
        searchfullname->setGeometry(QRect(230, 260, 94, 27));
        label_5 = new QLabel(PatientSearchDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(20, 220, 361, 21));
        cancel = new QPushButton(PatientSearchDialog);
        cancel->setObjectName(QString::fromUtf8("cancel"));
        cancel->setGeometry(QRect(10, 300, 94, 27));

        retranslateUi(PatientSearchDialog);

        QMetaObject::connectSlotsByName(PatientSearchDialog);
    } // setupUi

    void retranslateUi(QDialog *PatientSearchDialog)
    {
        PatientSearchDialog->setWindowTitle(QApplication::translate("PatientSearchDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("PatientSearchDialog", "First Name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("PatientSearchDialog", "Health Card", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        hcard->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("PatientSearchDialog", "Last Name", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("PatientSearchDialog", "Patient Search", 0, QApplication::UnicodeUTF8));
        searchfname->setText(QApplication::translate("PatientSearchDialog", "First Name", 0, QApplication::UnicodeUTF8));
        searchlname->setText(QApplication::translate("PatientSearchDialog", "Last Name", 0, QApplication::UnicodeUTF8));
        searchhcard->setText(QApplication::translate("PatientSearchDialog", "Health Card", 0, QApplication::UnicodeUTF8));
        searchfullname->setText(QApplication::translate("PatientSearchDialog", "Full Name", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("PatientSearchDialog", "Search by: (in most cases Health Card is sufficient)", 0, QApplication::UnicodeUTF8));
        cancel->setText(QApplication::translate("PatientSearchDialog", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PatientSearchDialog: public Ui_PatientSearchDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PATIENTSEARCHDIALOG_H
