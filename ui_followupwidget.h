/********************************************************************************
** Form generated from reading UI file 'followupwidget.ui'
**
** Created: Fri Nov 23 15:50:54 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FOLLOWUPWIDGET_H
#define UI_FOLLOWUPWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateEdit>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FollowupWidget
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *patientfname;
    QLineEdit *patientID;
    QLabel *label_3;
    QLabel *label_9;
    QLineEdit *patientlname;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_4;
    QLineEdit *physfname;
    QLineEdit *physID;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *physlname;
    QWidget *formLayoutWidget_3;
    QFormLayout *formLayout_3;
    QLabel *label_5;
    QTextEdit *description;
    QWidget *formLayoutWidget_4;
    QFormLayout *formLayout_4;
    QLabel *label_6;
    QLabel *label_8;
    QDateEdit *date;
    QComboBox *status;
    QWidget *formLayoutWidget_5;
    QFormLayout *formLayout_5;
    QLabel *label_7;
    QTextEdit *results;
    QTableWidget *tableWidget;
    QPushButton *save;
    QPushButton *newfup;
    QLabel *label_2;
    QLabel *label_12;
    QLabel *label_13;

    void setupUi(QWidget *FollowupWidget)
    {
        if (FollowupWidget->objectName().isEmpty())
            FollowupWidget->setObjectName(QString::fromUtf8("FollowupWidget"));
        FollowupWidget->resize(746, 686);
        formLayoutWidget = new QWidget(FollowupWidget);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(39, 50, 241, 101));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        patientfname = new QLineEdit(formLayoutWidget);
        patientfname->setObjectName(QString::fromUtf8("patientfname"));

        formLayout->setWidget(0, QFormLayout::FieldRole, patientfname);

        patientID = new QLineEdit(formLayoutWidget);
        patientID->setObjectName(QString::fromUtf8("patientID"));

        formLayout->setWidget(2, QFormLayout::FieldRole, patientID);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_9);

        patientlname = new QLineEdit(formLayoutWidget);
        patientlname->setObjectName(QString::fromUtf8("patientlname"));

        formLayout->setWidget(1, QFormLayout::FieldRole, patientlname);

        formLayoutWidget_2 = new QWidget(FollowupWidget);
        formLayoutWidget_2->setObjectName(QString::fromUtf8("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(310, 50, 251, 101));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(formLayoutWidget_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        physfname = new QLineEdit(formLayoutWidget_2);
        physfname->setObjectName(QString::fromUtf8("physfname"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, physfname);

        physID = new QLineEdit(formLayoutWidget_2);
        physID->setObjectName(QString::fromUtf8("physID"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, physID);

        label_10 = new QLabel(formLayoutWidget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_10);

        label_11 = new QLabel(formLayoutWidget_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_11);

        physlname = new QLineEdit(formLayoutWidget_2);
        physlname->setObjectName(QString::fromUtf8("physlname"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, physlname);

        formLayoutWidget_3 = new QWidget(FollowupWidget);
        formLayoutWidget_3->setObjectName(QString::fromUtf8("formLayoutWidget_3"));
        formLayoutWidget_3->setGeometry(QRect(40, 170, 241, 131));
        formLayout_3 = new QFormLayout(formLayoutWidget_3);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(formLayoutWidget_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_5);

        description = new QTextEdit(formLayoutWidget_3);
        description->setObjectName(QString::fromUtf8("description"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, description);

        formLayoutWidget_4 = new QWidget(FollowupWidget);
        formLayoutWidget_4->setObjectName(QString::fromUtf8("formLayoutWidget_4"));
        formLayoutWidget_4->setGeometry(QRect(40, 309, 241, 71));
        formLayout_4 = new QFormLayout(formLayoutWidget_4);
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        formLayout_4->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_6);

        label_8 = new QLabel(formLayoutWidget_4);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout_4->setWidget(2, QFormLayout::LabelRole, label_8);

        date = new QDateEdit(formLayoutWidget_4);
        date->setObjectName(QString::fromUtf8("date"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, date);

        status = new QComboBox(formLayoutWidget_4);
        status->setObjectName(QString::fromUtf8("status"));

        formLayout_4->setWidget(2, QFormLayout::FieldRole, status);

        formLayoutWidget_5 = new QWidget(FollowupWidget);
        formLayoutWidget_5->setObjectName(QString::fromUtf8("formLayoutWidget_5"));
        formLayoutWidget_5->setGeometry(QRect(309, 169, 251, 131));
        formLayout_5 = new QFormLayout(formLayoutWidget_5);
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        formLayout_5->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(formLayoutWidget_5);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_7);

        results = new QTextEdit(formLayoutWidget_5);
        results->setObjectName(QString::fromUtf8("results"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, results);

        tableWidget = new QTableWidget(FollowupWidget);
        if (tableWidget->columnCount() < 4)
            tableWidget->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(40, 440, 671, 192));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->setSortingEnabled(true);
        tableWidget->horizontalHeader()->setDefaultSectionSize(140);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        save = new QPushButton(FollowupWidget);
        save->setObjectName(QString::fromUtf8("save"));
        save->setGeometry(QRect(310, 310, 94, 27));
        newfup = new QPushButton(FollowupWidget);
        newfup->setObjectName(QString::fromUtf8("newfup"));
        newfup->setGeometry(QRect(440, 310, 121, 27));
        label_2 = new QLabel(FollowupWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(40, 20, 64, 17));
        label_12 = new QLabel(FollowupWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(310, 20, 81, 17));
        label_13 = new QLabel(FollowupWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(40, 420, 101, 17));

        retranslateUi(FollowupWidget);

        QMetaObject::connectSlotsByName(FollowupWidget);
    } // setupUi

    void retranslateUi(QWidget *FollowupWidget)
    {
        FollowupWidget->setWindowTitle(QApplication::translate("FollowupWidget", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("FollowupWidget", "First Name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("FollowupWidget", "PatientID:", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("FollowupWidget", "Last Name", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("FollowupWidget", "PhysicianID", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("FollowupWidget", "First Name", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("FollowupWidget", "Last Name", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("FollowupWidget", "Description", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("FollowupWidget", "Due Date", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("FollowupWidget", "Status", 0, QApplication::UnicodeUTF8));
        status->clear();
        status->insertItems(0, QStringList()
         << QApplication::translate("FollowupWidget", "Pending", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FollowupWidget", "Overdue", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FollowupWidget", "Results received", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FollowupWidget", "Complete", 0, QApplication::UnicodeUTF8)
        );
        label_7->setText(QApplication::translate("FollowupWidget", "Results", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("FollowupWidget", "Date", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("FollowupWidget", "Status", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("FollowupWidget", "Description", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("FollowupWidget", "Results", 0, QApplication::UnicodeUTF8));
        save->setText(QApplication::translate("FollowupWidget", "Save", 0, QApplication::UnicodeUTF8));
        newfup->setText(QApplication::translate("FollowupWidget", "New Followup", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("FollowupWidget", "Patient", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("FollowupWidget", "Physician", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("FollowupWidget", "Follow up List", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FollowupWidget: public Ui_FollowupWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FOLLOWUPWIDGET_H
