#include "patient.h"
#include "address.h"


QDataStream & operator << (QDataStream & stream, Patient & patient){
    QString num;
    num.setNum(patient.patientID);
    stream << num;
    stream << patient.healthcardno << patient.healthinsuranceno;
    stream << patient.firstName <<patient.lastName <<patient.dateOfBirth;
    stream << patient.comments;
    stream << patient.address << patient.contactinfo;
    return stream;
}

QDataStream & operator >> (QDataStream & stream, Patient & patient){
    QString num;
    stream >> num;
    patient.patientID = atoi(num.toStdString().c_str());

    stream >> patient.healthcardno;
    stream >> patient.healthinsuranceno;
    stream >> patient.firstName;
    stream >> patient.lastName >> patient.dateOfBirth;
    stream >> patient.comments;
    stream >> patient.address >> patient.contactinfo;
    return stream;
}

Patient::Patient(){}

Patient::Patient(QString healthcardno, QString healthinsuranceno,
                 QString firstName, QString lastName, Address &address, ContactInfo &contactinfo)
    : address(address), contactinfo(contactinfo)
{
    this->healthinsuranceno = healthinsuranceno;
    this->healthcardno = healthcardno;
    this->firstName = firstName;
    this->lastName = lastName;
    this->dateOfBirth = dateOfBirth;
}

//getters
qint32 Patient::getPatientID() const{
    return patientID;
}

qint32 Patient::getPrimaryPhysicianID() const{
    return primaryPhyID;
}

const QDate &Patient::getDateOfBirth() const{
    return dateOfBirth;
}
const QString &Patient::getHealthcardNo() const{
    return healthcardno;
}
const QString &Patient::getHealthInsuranceNo() const{
    return healthcardno;
}
const QString &Patient::getFirstName() const{
    return firstName;
}
const QString &Patient::getLastName() const{
    return lastName;
}
const QString &Patient::getComments() const{
    return comments;
}

const Address &Patient::getAddress() {
    return address;
}

const ContactInfo &Patient::getContactInfo() {
    return contactinfo;
}

const QList<Consultation> &Patient::getConsultations () const {
    return consultations;
}

//setters
void Patient::setDateOfBirth(QDate &dob) {
    this->dateOfBirth = dob;
}

void Patient::setPatientID(qint32 patientID) {
    this->patientID = patientID;
}

void Patient::setPrimaryPhysicianID(qint32 pysID) {
    this->primaryPhyID = pysID;
}

void Patient::setHealthcardNo(QString &healthcardno){
    this->healthcardno = healthcardno;
}

void Patient::setHealthInsuranceNo(QString &healthinsuranceno){
    this->healthinsuranceno = healthinsuranceno;
}

void Patient::setFirstName(QString &firstName){
    this->firstName = firstName;
}

void Patient::setLastName(QString &lastName){
    this->lastName = lastName;
}

void Patient::setComments(QString &comments){
    this->comments = comments;
}

void Patient::setAddress(Address & address){
    this->address = address;
}

void Patient::setContactInfo(ContactInfo & contactinfo){
    this->contactinfo = contactinfo;
}

void Patient::setConsultations(QList<Consultation> & consultations) {
    this->consultations = consultations;
}

void Patient::addConsultation (Consultation & consultation) {
    this->consultation = consultation;
}

//utility
void Patient::print(QString & str){
   QString num;
    str.append("\nPatient:\nFirstName: ").append(firstName).append("\nLastName: ").append(lastName);
    str.append("\nPatientID: ").append(num.setNum(patientID)).append("\nHealthCard: ");
    str.append(healthcardno).append("\nHealthInsurance: ").append(healthinsuranceno).append("\n");
    address.print(str);
    contactinfo.print(str);

}
