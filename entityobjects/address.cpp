#include "address.h"
#include <cstdlib>

QDataStream & operator << (QDataStream & stream, Address & address){
    stream << address.addressLineOne << address.addressLineTwo;
    stream << address.city;
    QString num;
    num.setNum(address.province);
    stream << num << address.postalCode;
    return stream;
}

QDataStream & operator >> (QDataStream & stream, Address & address){
    stream >> address.addressLineOne >> address.addressLineTwo;
    stream >> address.city;
    QString num;
    stream >> num;
    address.province = atoi(num.toStdString().c_str());
    stream >> address.postalCode;
    return stream;
}

Address::Address(){ }

Address::Address( QString addressLineOne
                , QString addressLineTwo
                , QString city
                , qint32 province
                , QString postalCode)
{
    this->addressLineOne = addressLineOne;
    this->addressLineTwo = addressLineTwo;
    this->city = city;
    this->province = province;
    this->postalCode = postalCode;
}


//getters
const QString &Address::getAddressLineOne() const{
    return addressLineOne;
}

const QString &Address::getAddressLineTwo() const{
    return addressLineTwo;
}

const QString &Address::getCity() const{
    return city;
}

qint32 Address::getProvince() const{
    return province;
}

const QString &Address::getPostalCode() const{
    return postalCode;
}

//setters
void Address::setAddressLineOne(QString & addressLineOne) {
    this->addressLineOne = addressLineOne;
}

void Address::setAddressLineTwo(QString & addressLineTwo) {
    this->addressLineTwo = addressLineTwo;
}

void Address::setCity(QString &city){
    this->city = city;
}

void Address::setProvince(qint32 province) {
    this->province = province;
}

void Address::setPostalCode(QString &postalCode){
    this->postalCode = postalCode;
}

//utility
void Address::print(QString &str){
    str.append(addressLineOne).append("\n");
    str.append(addressLineTwo).append("\n");
    str.append(city).append(", ").append(province).append("\n").append(postalCode).append("\n");
}
