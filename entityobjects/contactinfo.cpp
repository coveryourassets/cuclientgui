#include "contactinfo.h"
#include <iostream>

using namespace std;


QDataStream & operator << (QDataStream & stream, ContactInfo & info){
    stream << info.ext << info.cellNo << info.homeNo;
    stream << info.officeNo << info.primaryNo;
    return stream;

}

QDataStream & operator >> (QDataStream & stream, ContactInfo & info){
    stream >> info.ext >> info.cellNo >> info.homeNo;
    stream >> info.officeNo >> info.primaryNo;
    return stream;

}

ContactInfo::ContactInfo() { }

ContactInfo::ContactInfo(QString ext,QString cell, QString home,
                         QString office, QString primary){
    this->ext = ext;
    this->cellNo = cell;
    this->homeNo = home;
    this->officeNo = office;
    this->primaryNo = primary;
}


void ContactInfo::setExt (QString const &sParam) { this->ext = sParam; }
void ContactInfo::setCellNo (QString const &sParam) { this->cellNo = sParam; }
void ContactInfo::setHomeNo (QString const &sParam) { this->homeNo = sParam; }
void ContactInfo::setOfficeNo (QString const &sParam) { this->officeNo = sParam; }
void ContactInfo::setPrimaryNo (QString const &sParam) { this->primaryNo = sParam; }

QString ContactInfo::getExt () const { return ext; }
QString ContactInfo::getCellNo () const { return cellNo; }
QString ContactInfo::getHomeNo () const { return homeNo; }
QString ContactInfo::getOfficeNo () const { return officeNo; }
QString ContactInfo::getPrimaryNo () const { return primaryNo; }

//utility
void ContactInfo::print(QString &p){
    cout <<"in print contact"<<endl;
    p.append("Contact info:\n");
    p.append(ext).append("\n");
    p.append(cellNo).append("\n");
    p.append(homeNo).append("\n");
    p.append(officeNo).append("\n");
    p.append(primaryNo).append("\n");

}
