#ifndef CONTACTINFO_H
#define CONTACTINFO_H

#include <QString>

class ContactInfo
{
public:

    friend QDataStream & operator << (QDataStream & stream, ContactInfo & info);
    friend QDataStream & operator >> (QDataStream & stream, ContactInfo & info);

    ContactInfo();
    ContactInfo(QString ext,QString cell, QString home, QString office, QString primary);

    void setExt (QString const &sParam);
    void setCellNo (QString const &sParam);
    void setHomeNo (QString const &sParam);
    void setOfficeNo (QString const &sParam);
    void setPrimaryNo (QString const &sParam);

    QString getExt () const;
    QString getCellNo () const;
    QString getHomeNo () const;
    QString getOfficeNo () const;
    QString getPrimaryNo () const;

    //utility
    void print(QString &p);

private:

    QString officeNo;
    QString homeNo;
    QString cellNo;
    QString ext;
    QString primaryNo;
};

#endif // CONTACTINFO_H
