#ifndef FOLLOWUP_H
#define FOLLOWUP_H

#include <iostream>
#include <time.h>
#include <QDateTime>
#include <QDataStream>

using namespace std;

enum Status {pending, overdue, results_received, complete};

class Followup
{

    friend QDataStream & operator << (QDataStream & stream, Followup & followup);

    friend QDataStream & operator >> (QDataStream & stream, Followup & followup);


public:
    Followup();
    Followup(QString description, QDate duedate, qint32 consultID);

    //getters
    Status getStatus() const;

    qint32 getFollowupID() const;
    qint32 getConsultID() const;

    const QString &getDescription() const;
    const QDate &getDate() const;
    const QString &getResults() const;

    //setters
    void setFollowupID(qint32 followupID);
    void setConsultID(qint32 consultID);
    void setDescription(QString &description);
    void setStatus(Status status);
    void setDate(QDate date);
    void setResults(QString &results);

private:

    Status status;

    qint32 followupID;
    qint32 consultID;

    QString description;
    QDate duedate;
    QString results;
};

#endif // FOLLOWUP_H
