#ifndef CONSULTATION_H
#define CONSULTATION_H

#include <string>
#include <cstddef>
#include <vector>
#include <time.h>
#include <QDataStream>
#include <QDateTime>
#include "followup.h"

using namespace std;

class Consultation
{
    friend QDataStream & operator << (QDataStream & stream, Consultation & consult);
    friend QDataStream & operator >> (QDataStream & stream, Consultation & consult);

public:
    Consultation();
    Consultation( qint32 physID
                , qint32 patientID
                , QDateTime date
                , QString reason);

    //getters
    qint32 getConsultID() const;
    qint32 getPhysID() const;
    qint32 getPatientID() const;
    const QString &getPhysFirstName() const;
    const QString &getPhysLastName() const;

    const QDateTime getDate() const;

   // const QString &getHealthcardNo() const;
   // const QString &getHealthInsuranceNo() const;
    const QString &getReason() const;
    const QString &getDiagnosis() const;

    QList<Followup> &getFollowups();

    //setters
    void setConsultID(qint32 consultID);
    void setPhysID(qint32 physID);
    void setPatientID(qint32 physID);

    void setDate(QDateTime &date);

    void setPhysFirstName(QString &fname);
    void setPhysLastName(QString &lname);
    void setReason(QString &reason);
    void setDiagnosis(QString &diagnosis);
    void addFollowup(Followup &followup);
    void setFollowups(QList<Followup> &followups);

    void print(QString &str);

private:
    qint32 consultID;
    qint32 physID;
    qint32 patientID;

    QString physFirstName;
    QString physLastName;
    QDateTime date;
    QString reason;
    QString diagnosis;
    QList<Followup> followups;
};

#endif // CONSULTATION_H
