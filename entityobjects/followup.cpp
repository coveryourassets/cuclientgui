#include "followup.h"

QDataStream & operator << (QDataStream & stream, Followup & followup){

    stream << followup.followupID << followup.description <<followup.consultID;
    stream << (qint32)followup.status << followup.duedate << followup.results;
    return stream;
}

QDataStream & operator >> (QDataStream & stream, Followup & followup){

    stream >> (qint32&)followup.followupID >> followup.description << followup.consultID;
    stream >> (qint32&)followup.status >> followup.duedate >> followup.results;
    return stream;

}


Followup::Followup(){
}

Followup::Followup(QString description, QDate duedate, qint32 consultID)
{
    this->description = description;
    this->duedate = duedate;
    this->consultID = consultID;
    status = pending;
}

//getters
qint32 Followup::getFollowupID()const{
    return followupID;
}

qint32 Followup::getConsultID()const{
    return consultID;
}

const QString &Followup::getDescription() const{
    return description;
}

Status Followup::getStatus() const{
    return status;
}

const QDate &Followup::getDate() const{
    return duedate;
}

const QString &Followup::getResults() const{
    return results;
}

//setters
void Followup::setFollowupID(qint32 followupID){
    this->followupID = followupID;
}

void Followup::setConsultID(qint32 consultID){
    this->consultID = consultID;
}

void Followup::setDescription(QString &description){
    this->description = description;
}

void Followup::setStatus(Status status){
    this->status = status;
}

void Followup::setDate(QDate date){
    this->duedate = date;
}

void Followup::setResults(QString &results){
    this->results = results;
}

