/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri Nov 23 15:50:54 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QLabel *message;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QPushButton *searchfname;
    QLineEdit *fname;
    QPushButton *searchlname;
    QLineEdit *lname;
    QPushButton *searchhcard;
    QLineEdit *healthcard;
    QLabel *label_2;
    QPushButton *searchfullname;
    QPushButton *newpatient;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(90, 0, 311, 51));
        QFont font;
        font.setFamily(QString::fromUtf8("Bitstream Charter"));
        font.setPointSize(24);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        message = new QLabel(centralwidget);
        message->setObjectName(QString::fromUtf8("message"));
        message->setGeometry(QRect(200, 260, 281, 17));
        formLayoutWidget = new QWidget(centralwidget);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(90, 120, 341, 121));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        searchfname = new QPushButton(formLayoutWidget);
        searchfname->setObjectName(QString::fromUtf8("searchfname"));

        formLayout->setWidget(1, QFormLayout::LabelRole, searchfname);

        fname = new QLineEdit(formLayoutWidget);
        fname->setObjectName(QString::fromUtf8("fname"));

        formLayout->setWidget(1, QFormLayout::FieldRole, fname);

        searchlname = new QPushButton(formLayoutWidget);
        searchlname->setObjectName(QString::fromUtf8("searchlname"));

        formLayout->setWidget(2, QFormLayout::LabelRole, searchlname);

        lname = new QLineEdit(formLayoutWidget);
        lname->setObjectName(QString::fromUtf8("lname"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lname);

        searchhcard = new QPushButton(formLayoutWidget);
        searchhcard->setObjectName(QString::fromUtf8("searchhcard"));

        formLayout->setWidget(3, QFormLayout::LabelRole, searchhcard);

        healthcard = new QLineEdit(formLayoutWidget);
        healthcard->setObjectName(QString::fromUtf8("healthcard"));

        formLayout->setWidget(3, QFormLayout::FieldRole, healthcard);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(90, 90, 141, 21));
        searchfullname = new QPushButton(centralwidget);
        searchfullname->setObjectName(QString::fromUtf8("searchfullname"));
        searchfullname->setGeometry(QRect(440, 140, 85, 27));
        newpatient = new QPushButton(centralwidget);
        newpatient->setObjectName(QString::fromUtf8("newpatient"));
        newpatient->setGeometry(QRect(90, 260, 97, 27));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Welcome to cuCare", 0, QApplication::UnicodeUTF8));
        message->setText(QString());
        searchfname->setText(QApplication::translate("MainWindow", "First Name", 0, QApplication::UnicodeUTF8));
        searchlname->setText(QApplication::translate("MainWindow", "Last Name", 0, QApplication::UnicodeUTF8));
        searchhcard->setText(QApplication::translate("MainWindow", "Healthcard", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Patient Search By:", 0, QApplication::UnicodeUTF8));
        searchfullname->setText(QApplication::translate("MainWindow", "Full Name", 0, QApplication::UnicodeUTF8));
        newpatient->setText(QApplication::translate("MainWindow", "New Patient", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
