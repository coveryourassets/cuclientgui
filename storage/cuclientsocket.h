#ifndef CUCLIENTSOCKET_H
#define CUCLIENTSOCKET_H

#include "../entityobjects/consultation.h"
#include "../entityobjects/patient.h"
#include "../entityobjects/user.h"
#include "../entityobjects/user.h"
#include <sstream>
#include "custore.h"
#include "client.h"

class cuClientSocket : public cuStore
{
public:
    cuClientSocket();
    //consultation records

    //given a patient, return the list of associated consultations. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the consultations to those with followups
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType getConsultationList(Patient &patient, QList<Consultation> &consults, int results = 0);

    //save a new consultation.
    errorType saveNewConsultation(Consultation &);

    //update a consultation
    errorType updateConsultation(Consultation &);

    //followups

    //given a consultation, return the list of associated followups. The
    //calling function provides an empty QList that gets populated. The
    //results flag is if we limit the followups to those
    //whose results have been received, in which case we set it to 1. Otherwise
    //it defaults to 0
    errorType getFollowupList(Consultation &consults, QList<Followup> &followups, int results = 0);

    //save a new followup
    errorType saveNewFollowup(Followup &followup);

    //update a followup
    errorType updateFollowup(Followup & followup);

    //patient records

    //given a firstname, lastname, or both, return the list of associated Patients. The
    //calling function provides an empty QList that gets populated
    errorType searchPatientbyFirstName(QString firstname, QList<Patient> &patients);
    errorType searchPatientbyLastName(QString lastname, QList<Patient> &patients);
    errorType searchPatientbyFullName(QString lastname, QString firstname, QList<Patient> &patients);
    errorType searchPatientbyFollowup(Followup& followup, Patient &patient);


    //given a healthcardno, return the list (there should only be one) of patients. The
    //calling function provides an empty QList that gets populated
    errorType searchPatientbyHealthcard(QString healthcardno, QList<Patient> &patients);

    //return patients who have recieved results, filtered by physician (optional)
    errorType searchPatientsWithResultsReceived(QList<Patient> &patients, int physID = -1);

    errorType getFollowupsWithRR(QList<Followup> &followups);


    //not part of the requirements, but used in testing
    errorType saveNewPatient(Patient & patient);
    errorType updatePatient(Patient & patient);


    //user records

    //Given a userName, return the user. The calling function provides
    //an empty user class to populate
    errorType getUser(QString userName, User &user);

    errorType updateUser(User &user);

    //for testing purposes
    errorType saveNewUser(User &user);

    //other
    errorType createDatabase();

    //testing
    errorType testPrepared();

private:
    Client client;
    errorType error;
};

#endif // CUCLIENTSOCKET_H
