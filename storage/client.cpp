#include "client.h"
#include <iostream>
#include <QHostAddress>
#include <QHostAddress>

using namespace std;

Client::Client()
{
}

QByteArray &Client:: startWrite(errorType &error){
    buffer.clear();
    QHostAddress add;
    add.setAddress("127.0.0.1");
    socket.connectToHost(add, 8888);
    int rc = socket.waitForConnected(30);
    if (!rc){
        cout << "Could not connect to server"<<endl;
        error = generalError;
    }else{
        error = OK;
    }
    return buffer;
}

void Client::finishWrite(){
    socket.write(buffer);
    socket.flush();
}

QByteArray & Client::startRead(){
    buffer.clear();
    int rc = socket.waitForReadyRead(-1);
    if(!rc){
        cout << "Network timed out"<<endl;
    }
    buffer = socket.readAll();
    return buffer;
}

void Client::finishRead(){
    socket.disconnectFromHost();
}
