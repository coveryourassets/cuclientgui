#ifndef CLIENT_H
#define CLIENT_H

#include <QTcpSocket>
#include "custore.h"

class Client
{
public:
    Client();

    QByteArray & startWrite(errorType &error);
    void finishWrite();
    QByteArray & startRead();
    void finishRead();

private:
    QTcpSocket socket;
    QByteArray buffer;
};

#endif // CLIENT_H
