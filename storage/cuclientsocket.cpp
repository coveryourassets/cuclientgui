#include "cuclientsocket.h"

cuClientSocket::cuClientSocket()
{
}

//consultation records

//given a patient, return the list of associated consultations. The
//calling function provides an empty QList that gets populated. The
//results flag is if we limit the consultations to those with followups
//whose results have been received, in which case we set it to 1. Otherwise
//it defaults to 0
errorType cuClientSocket::getConsultationList(Patient &patient, QList<Consultation> &consults, int results){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << getConsultList << patient;
    client.finishWrite();

    //read
    qDebug("getConsultList, reading data");
    QDataStream in(client.startRead());
    qDebug("getConsultList, finished read");
    qint32 num;
    in >> num;
    qDebug("socket client, %d consultations being received", num);

    Consultation *c;
    QString con;
    for (int i = 0; i < num; i ++){
        c = new Consultation();
        qDebug("finished making empty consultation");
        in >> *c;
        qDebug("finished reading in consultation");
        consults.push_back(*c);
        qDebug("finished adding consultation to the list");
        c->print(con);
        qDebug(con.toStdString().c_str());
    }
    client.finishRead();
    return OK;
}

//save a new consultation.
errorType cuClientSocket::saveNewConsultation(Consultation & consult){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << saveNewConsult << consult;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}

//update a consultation
errorType cuClientSocket::updateConsultation(Consultation & consult){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << updateConsult << consult;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}

//followups

//given a consultation, return the list of associated followups. The
//calling function provides an empty QList that gets populated. The
//results flag is if we limit the followups to those
//whose results have been received, in which case we set it to 1. Otherwise
//it defaults to 0
errorType cuClientSocket::getFollowupList(Consultation &consult, QList<Followup> &followups, int results){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << getFupList << consult;
    client.finishWrite();

    //read
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Followup * f;
    for (int i = 0; i < number; i ++){
        f = new Followup();
        in >> *f;
        followups.push_back(*f);
    }
    client.finishRead();
    return OK;
}

//save a new followup
errorType cuClientSocket::saveNewFollowup(Followup &followup){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << saveNewFup << followup;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}

//update a followup
errorType cuClientSocket::updateFollowup(Followup & followup){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << updateFup << followup;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}

//patient records

//given a firstname, lastname, or both, return the list of associated Patients. The
//calling function provides an empty QList that gets populated
errorType cuClientSocket::searchPatientbyFirstName(QString firstname, QList<Patient> &patients){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatFName << firstname;
    client.finishWrite();
    qDebug("Client finished writing");
    QDataStream in(&client.startRead(), QIODevice::ReadWrite);
    qint32 number;
    qDebug("Client reading number...");
    in >> number;
    qDebug("Done. Number of patients: %d", number);
    Patient * p;
    QString s;
    for (int i = 0; i < number; i ++){
        qDebug("In loop");
        p = new Patient();
        in >> *p;
        p->print(s);
        qDebug(s.toStdString().c_str());
        patients.push_back(*p);
    }
    client.finishRead();
    qDebug("Client finished reading");
    return OK;
}

errorType cuClientSocket::searchPatientbyLastName(QString lastname, QList<Patient> &patients){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatLName << lastname;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Patient * p;
    for (int i = 0; i < number; i ++){
        p = new Patient();
        in >> *p;
        patients.push_back(*p);
    }
    client.finishRead();
    return OK;
}

errorType cuClientSocket::searchPatientbyFullName(QString lastname, QString firstname, QList<Patient> &patients){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatFullName << lastname << firstname;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Patient * p;
    for (int i = 0; i < number; i ++){
        p = new Patient();
        in >> *p;
        patients.push_back(*p);
    }
    client.finishRead();
    return OK;
}

errorType cuClientSocket::searchPatientbyFollowup(Followup& followup, Patient &patient){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatByFup << followup;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    errorType t = (errorType)number;
    if (t == OK){
        in >> patient;
    }
    return t;
}

//given a healthcardno, return the list (there should only be one) of patients. The
//calling function provides an empty QList that gets populated
errorType cuClientSocket::searchPatientbyHealthcard(QString healthcardno, QList<Patient> &patients){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatHCN << healthcardno;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Patient * p;
    for (int i = 0; i < number; i ++){
        p = new Patient();
        in >> *p;
        patients.push_back(*p);
    }
    client.finishRead();
    return OK;
}

//return patients who have recieved results, filtered by physician (optional)
errorType cuClientSocket::searchPatientsWithResultsReceived(QList<Patient> &patients, int physID){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << searchPatWithRR << (qint32)physID;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Patient * p;
    for (int i = 0; i < number; i ++){
        p = new Patient();
        in >> *p;
        patients.push_back(*p);
    }
    client.finishRead();
    return OK;
}

errorType cuClientSocket::getFollowupsWithRR(QList<Followup> &followups){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << getFURR;
    client.finishWrite();
    QDataStream in(client.startRead());
    qint32 number;
    in >> number;
    Followup * f;
    for (int i = 0; i < number; i ++){
        f = new Followup();
        in >> *f;
        followups.push_back(*f);
    }
    client.finishRead();
    return OK;
}


//not part of the requirements, but used in testing
errorType cuClientSocket::saveNewPatient(Patient & patient){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << saveNewPat << patient;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;

}

errorType cuClientSocket::updatePatient(Patient & patient){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << updatePat << patient;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}


//user records

//Given a userName, return the user. The calling function provides
//an empty user class to populate
errorType cuClientSocket::getUser(QString userName, User &user){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << getUsr << userName;
    client.finishWrite();
    QDataStream in(client.startRead());
    in >> user;
    client.finishRead();
    return OK;
}

errorType cuClientSocket::updateUser(User &user){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << updateUsr << user;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;
}

//for testing purposes
errorType cuClientSocket::saveNewUser(User &user){
    QDataStream out(&client.startWrite(error), QIODevice::WriteOnly);
    if (error != OK){
        return error;
    }
    out << saveNewUsr << user;
    client.finishWrite();
    QDataStream in(client.startRead());
    errorType type;
    qint32 t;
    in >> t;
    client.finishRead();
    type = (errorType)t;
    return type;

}

//other
errorType cuClientSocket::createDatabase(){
    return OK;
}

//testing
errorType cuClientSocket::testPrepared(){
    return OK;
}
