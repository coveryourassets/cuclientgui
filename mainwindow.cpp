#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(MainController *control, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->user = control->getUser();
    this->storage = control->getStorage();
    this->patients = control->getPatients();
    this->control = control;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete user;
}

void MainWindow::swapWidget(QWidget *widget){
    tempwidget = ui->centralwidget;
    ui->centralwidget = widget;
}

void MainWindow::on_newpatient_clicked()
{
    control->newPatient();
}

//search methods

void MainWindow::on_searchhcard_clicked()
{
    patients->clear();
    errorType t = storage->searchPatientbyHealthcard(ui->healthcard->text(), *patients);
    if (t!=OK){
        QMessageBox::information( this, "Patient Search",
                                  "Database error. Is the server running?",
                                    "OK",0);
        return;
    }
    if (patients->size() == 0){
        QMessageBox::information( this, "Patient Search",
                                  "No patients found by that criteria",
                                    "OK",0);
        return;
    }
    control->openPatientWindow();
}

void MainWindow::on_searchfname_clicked()
{
    patients->clear();
    errorType t = storage->searchPatientbyFirstName(ui->fname->text(), *patients);
    if (t!=OK){
        QMessageBox::information( this, "Patient Search",
                                  "Database error. Is the server running?",
                                    "OK",0);
        return;
    }
    if (patients->size() == 0){
        QMessageBox::information( this, "Patient Search",
                                  "No patients found by that criteria",
                                    "OK",0);
        return;
    }
    control->openPatientWindow();
}

void MainWindow::on_searchlname_clicked()
{
    qDebug("searching last name");
    patients->clear();
    qDebug("making query");
    errorType t = storage->searchPatientbyLastName(ui->lname->text(), *patients);
    if (t!=OK){
        QMessageBox::information( this, "Patient Search",
                                  "Database error. Is the server running?",
                                    "OK",0);
        return;
    }
    if (patients->size() == 0){
        QMessageBox::information( this, "Patient Search",
                                  "No patients found by that criteria",
                                    "OK",0);
        return;
    }
    qDebug("opening window");
    control->openPatientWindow();
}

void MainWindow::on_searchfullname_clicked()
{
    patients->clear();
    errorType t = storage->searchPatientbyFullName(ui->lname->text(),ui->fname->text(), *patients);
    if (t!=OK){
        QMessageBox::information( this, "Patient Search",
                                  "Database error. Is the server running?",
                                    "OK",0);
        return;
    }
    if (patients->size() == 0){
        QMessageBox::information( this, "Patient Search",
                                  "No patients found by that criteria",
                                    "OK",0);
        return;
    }
    control->openPatientWindow();
}


