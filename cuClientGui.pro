#-------------------------------------------------
#
# Project created by QtCreator 2012-11-15T12:20:58
#
#-------------------------------------------------

QT       += core gui network

TARGET = cuClientGui
TEMPLATE = app


SOURCES += main.cpp\
        patientwindow.cpp \
    entityobjects/user.cpp \
    entityobjects/patient.cpp \
    entityobjects/followup.cpp \
    entityobjects/contactinfo.cpp \
    entityobjects/consultation.cpp \
    entityobjects/address.cpp \
    storage/custore.cpp \
    storage/cuclientsocket.cpp \
    storage/client.cpp \
    logindialog.cpp \
    mainwindow.cpp \
    maincontroller.cpp \
    consultwindow.cpp \
    followupwindow.cpp \
    followupwidget.cpp

HEADERS  += patientwindow.h \
    entityobjects/user.h \
    entityobjects/patient.h \
    entityobjects/followup.h \
    entityobjects/contactinfo.h \
    entityobjects/consultation.h \
    entityobjects/address.h \
    storage/custore.h \
    storage/cuclientsocket.h \
    storage/client.h \
    logindialog.h \
    mainwindow.h \
    maincontroller.h \
    consultwindow.h \
    followupwindow.h \
    followupwidget.h

FORMS    += patientwindow.ui \
    logindialog.ui \
    mainwindow.ui \
    consultwindow.ui \
    followupwindow.ui \
    followupwidget.ui
