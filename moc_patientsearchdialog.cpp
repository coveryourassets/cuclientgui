/****************************************************************************
** Meta object code from reading C++ file 'patientsearchdialog.h'
**
** Created: Fri Nov 23 05:26:36 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "patientsearchdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'patientsearchdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PatientSearchDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,
      41,   20,   20,   20, 0x08,
      66,   20,   20,   20, 0x08,
      91,   20,   20,   20, 0x08,
     116,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PatientSearchDialog[] = {
    "PatientSearchDialog\0\0on_cancel_clicked()\0"
    "on_searchhcard_clicked()\0"
    "on_searchfname_clicked()\0"
    "on_searchlname_clicked()\0"
    "on_searchfullname_clicked()\0"
};

void PatientSearchDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PatientSearchDialog *_t = static_cast<PatientSearchDialog *>(_o);
        switch (_id) {
        case 0: _t->on_cancel_clicked(); break;
        case 1: _t->on_searchhcard_clicked(); break;
        case 2: _t->on_searchfname_clicked(); break;
        case 3: _t->on_searchlname_clicked(); break;
        case 4: _t->on_searchfullname_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData PatientSearchDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PatientSearchDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PatientSearchDialog,
      qt_meta_data_PatientSearchDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PatientSearchDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PatientSearchDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PatientSearchDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PatientSearchDialog))
        return static_cast<void*>(const_cast< PatientSearchDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int PatientSearchDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
