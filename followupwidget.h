#ifndef FOLLOWUPWIDGET_H
#define FOLLOWUPWIDGET_H

#include <QWidget>
#include "maincontroller.h"

class MainController;

namespace Ui {
class FollowupWidget;
}

class FollowupWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit FollowupWidget(MainController *control, QWidget *parent = 0);
    ~FollowupWidget();

    void initializeWithNewConsultation();
    
private slots:
    void on_tableWidget_clicked(const QModelIndex &index);

    void on_save_clicked();

private:
    void populateFollowupForm();
    void populateFollowupTable();

    Ui::FollowupWidget *ui;
    MainController * control;
    QList<Followup> * followups;
    Patient ** currentPatient;
    Patient * curpat;
    Consultation ** currentConsult;
    Followup ** currentFollowup;
    cuStore * storage;
};

#endif // FOLLOWUPWIDGET_H
