#include "followupwindow.h"
#include "ui_followupwindow.h"
#include "followupwidget.h"

FollowupWindow::FollowupWindow(MainController * control, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FollowupWindow)
{
    ui->setupUi(this);
   // ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    fupwidget = new FollowupWidget(control, this);
    ui->centralwidget = fupwidget;
    this->control = control;
}

FollowupWindow::~FollowupWindow()
{
    delete ui;
}

FollowupWidget *FollowupWindow::getFollowupWidget(){
    return fupwidget;
}
