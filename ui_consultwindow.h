/********************************************************************************
** Form generated from reading UI file 'consultwindow.ui'
**
** Created: Fri Nov 23 15:50:54 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONSULTWINDOW_H
#define UI_CONSULTWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConsultWindow
{
public:
    QWidget *centralwidget;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *physFName;
    QLabel *label_2;
    QLineEdit *physLName;
    QLineEdit *physID;
    QLabel *label_3;
    QLabel *label_5;
    QDateTimeEdit *date;
    QLabel *label_4;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QTextEdit *reason;
    QWidget *formLayoutWidget_3;
    QFormLayout *formLayout_3;
    QLabel *label_7;
    QTextEdit *diagnosis;
    QLabel *patientName;
    QPushButton *save;
    QPushButton *getfollowups;
    QPushButton *reset;
    QPushButton *back;
    QLabel *message;
    QTableWidget *tableWidget;
    QPushButton *newconsult;
    QPushButton *editconsult;
    QPushButton *newfollowup;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ConsultWindow)
    {
        if (ConsultWindow->objectName().isEmpty())
            ConsultWindow->setObjectName(QString::fromUtf8("ConsultWindow"));
        ConsultWindow->resize(800, 580);
        centralwidget = new QWidget(ConsultWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        formLayoutWidget = new QWidget(centralwidget);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 30, 333, 141));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        physFName = new QLineEdit(formLayoutWidget);
        physFName->setObjectName(QString::fromUtf8("physFName"));

        formLayout->setWidget(1, QFormLayout::FieldRole, physFName);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        physLName = new QLineEdit(formLayoutWidget);
        physLName->setObjectName(QString::fromUtf8("physLName"));

        formLayout->setWidget(2, QFormLayout::FieldRole, physLName);

        physID = new QLineEdit(formLayoutWidget);
        physID->setObjectName(QString::fromUtf8("physID"));

        formLayout->setWidget(3, QFormLayout::FieldRole, physID);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        label_5 = new QLabel(formLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        date = new QDateTimeEdit(formLayoutWidget);
        date->setObjectName(QString::fromUtf8("date"));

        formLayout->setWidget(4, QFormLayout::FieldRole, date);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 0, 121, 17));
        formLayoutWidget_2 = new QWidget(centralwidget);
        formLayoutWidget_2->setObjectName(QString::fromUtf8("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(370, 30, 311, 141));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_6);

        reason = new QTextEdit(formLayoutWidget_2);
        reason->setObjectName(QString::fromUtf8("reason"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, reason);

        formLayoutWidget_3 = new QWidget(centralwidget);
        formLayoutWidget_3->setObjectName(QString::fromUtf8("formLayoutWidget_3"));
        formLayoutWidget_3->setGeometry(QRect(10, 180, 321, 101));
        formLayout_3 = new QFormLayout(formLayoutWidget_3);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(formLayoutWidget_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_7);

        diagnosis = new QTextEdit(formLayoutWidget_3);
        diagnosis->setObjectName(QString::fromUtf8("diagnosis"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, diagnosis);

        patientName = new QLabel(centralwidget);
        patientName->setObjectName(QString::fromUtf8("patientName"));
        patientName->setGeometry(QRect(133, 0, 541, 20));
        save = new QPushButton(centralwidget);
        save->setObjectName(QString::fromUtf8("save"));
        save->setGeometry(QRect(480, 180, 94, 27));
        getfollowups = new QPushButton(centralwidget);
        getfollowups->setObjectName(QString::fromUtf8("getfollowups"));
        getfollowups->setGeometry(QRect(300, 300, 121, 27));
        reset = new QPushButton(centralwidget);
        reset->setObjectName(QString::fromUtf8("reset"));
        reset->setGeometry(QRect(370, 180, 94, 27));
        back = new QPushButton(centralwidget);
        back->setObjectName(QString::fromUtf8("back"));
        back->setGeometry(QRect(590, 180, 94, 27));
        message = new QLabel(centralwidget);
        message->setObjectName(QString::fromUtf8("message"));
        message->setGeometry(QRect(380, 220, 301, 17));
        tableWidget = new QTableWidget(centralwidget);
        if (tableWidget->columnCount() < 5)
            tableWidget->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 340, 661, 261));
        tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->horizontalHeader()->setDefaultSectionSize(130);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        newconsult = new QPushButton(centralwidget);
        newconsult->setObjectName(QString::fromUtf8("newconsult"));
        newconsult->setGeometry(QRect(10, 300, 131, 27));
        editconsult = new QPushButton(centralwidget);
        editconsult->setObjectName(QString::fromUtf8("editconsult"));
        editconsult->setGeometry(QRect(150, 300, 141, 27));
        newfollowup = new QPushButton(centralwidget);
        newfollowup->setObjectName(QString::fromUtf8("newfollowup"));
        newfollowup->setGeometry(QRect(430, 300, 111, 27));
        ConsultWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ConsultWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        ConsultWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(ConsultWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        ConsultWindow->setStatusBar(statusbar);

        retranslateUi(ConsultWindow);

        QMetaObject::connectSlotsByName(ConsultWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ConsultWindow)
    {
        ConsultWindow->setWindowTitle(QApplication::translate("ConsultWindow", "Consultation Window", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ConsultWindow", "Physician First Name", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ConsultWindow", "Physician Last Name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ConsultWindow", "Physician ID", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("ConsultWindow", "Date and Time", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("ConsultWindow", "Consultation for", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("ConsultWindow", "Reason for Visit", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("ConsultWindow", "Diagnosis", 0, QApplication::UnicodeUTF8));
        patientName->setText(QString());
        save->setText(QApplication::translate("ConsultWindow", "Save", 0, QApplication::UnicodeUTF8));
        getfollowups->setText(QApplication::translate("ConsultWindow", "Get Follow-ups", 0, QApplication::UnicodeUTF8));
        reset->setText(QApplication::translate("ConsultWindow", "Reset", 0, QApplication::UnicodeUTF8));
        back->setText(QApplication::translate("ConsultWindow", "Back", 0, QApplication::UnicodeUTF8));
        message->setText(QString());
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ConsultWindow", "PhysicianID", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ConsultWindow", "Physician", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("ConsultWindow", "Date", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("ConsultWindow", "Reason", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("ConsultWindow", "Diagnosis", 0, QApplication::UnicodeUTF8));
        newconsult->setText(QApplication::translate("ConsultWindow", "New Consultion", 0, QApplication::UnicodeUTF8));
        editconsult->setText(QApplication::translate("ConsultWindow", "Edit Consultation", 0, QApplication::UnicodeUTF8));
        newfollowup->setText(QApplication::translate("ConsultWindow", "New Follow-up", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConsultWindow: public Ui_ConsultWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONSULTWINDOW_H
