#ifndef PATIENTWINDOW_H
#define PATIENTWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include "entityobjects/patient.h"
#include "storage/cuclientsocket.h"
#include "maincontroller.h"

class MainController;

namespace Ui {
class PatientWindow;
}

class PatientWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PatientWindow(MainController *control, QWidget *parent = 0);
    ~PatientWindow();


public slots:

    void populatePatientForm();
    void populatePatientTable();


private slots:
    void on_createPatient_clicked();

    void on_savePatient_clicked();

    void on_tableWidget_doubleClicked(const QModelIndex &index);

    void on_getConsults_clicked();

    void on_clear_clicked();

    void on_exit_clicked();

    void clearPatientForm();

    void update();

private:
    Ui::PatientWindow *ui;
    QList<Patient> *patients;
    Patient ** currentPatient;
    cuStore* storage;
    User *user;
    MainController * control;
};

#endif // MainWindow_H
