#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(cuStore * storage, User *user, MainController *main, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    this->storage = storage;
    this->user = user;
    this->main = main;
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_buttonBox_accepted()
{
    User user;
    user.setUserID(-1);
    errorType t = storage->getUser(ui->uname->text(), user);
    if (t == generalError){
        //you fail
        reject();
    }else{
        //success
        accept();
    }
    main->loginSuccess();

}
