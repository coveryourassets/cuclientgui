#include "consultwindow.h"
#include "ui_consultwindow.h"
#include "followupwidget.h"

ConsultWindow::ConsultWindow(MainController *control, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConsultWindow)
{
    ui->setupUi(this);
    this->control = control;
    this->user=control->getUser();
    this->storage = control->getStorage();
    currentConsult = control->getCurrentConsult();
    currentPatient = control->getCurrentPatient();
    followups = control->getFollowups();
    consults = control->getConsults();
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
}

ConsultWindow::~ConsultWindow()
{
    delete ui;
}


void ConsultWindow::initializeWithNewPatient(){
    QString name((*currentPatient)->getFirstName());
    name.append(" ").append((*currentPatient)->getLastName());
    name.append("       Healthcard Number: ").append((*currentPatient)->getHealthcardNo());
    ui->patientName->setText(name);
    qDebug("patient information is set");
    storage->getConsultationList(**currentPatient, *consults, 0);
    qDebug("database has returned, populating consult table");
    populateConsultTable();
    qDebug("consult table populated, showing");
    show();
}


void ConsultWindow::setCurrentConsult(Consultation *consult){
    *currentConsult = consult;
}

Consultation **ConsultWindow::getCurrentConsult(){
    return currentConsult;
}


void ConsultWindow::on_save_clicked()
{
    //sanity check
    if ((*currentPatient)==0){
        QMessageBox::warning( this, "Warning",
                              "Please return to patient window to select patient",
                              "OK",0);
        return;
    }
    QString message;
    QString button;
    if (*currentConsult == 0){
        message.append("Save consultation?");
        button.append("Save");
    }else{
        message.append("Update consultation?");
        button.append("Update");
    }
    if( QMessageBox::information( this, "Consultation Save",
                                          message,
                                          button, "Cancel",
                                          0, 1 ) == 1) {


        //user pressed cancel
        qDebug("Cancelling");
        return;
    }

    qint32 physId = ui->physID->text().toInt();
    QString pfname(ui->physFName->text());
    QString plname(ui->physLName->text());
    QString diagnosis(ui->diagnosis->toPlainText());
    qDebug("read in QStrings");

    (*currentConsult) = new Consultation(physId,(*currentPatient)->getPatientID(),ui->date->dateTime(),ui->reason->toPlainText());
    qDebug("made new consultation");

    (*currentConsult)->setPhysFirstName(pfname);
    (*currentConsult)->setPhysLastName(plname);
    (*currentConsult)->setDiagnosis(diagnosis);

    qDebug("set consult params");


    QString p;
    (*currentConsult)->print(p);
    qDebug(p.toStdString().c_str());
    qDebug("consultation printed");

    errorType t = storage->saveNewConsultation(**currentConsult);
    if (t==generalError){
        ui->message->setText(QString("consultation not saved, try again later"));
        delete (*currentConsult);
        (*currentConsult) = 0;
    }else{
        ui->message->setText(QString("new consultation saved"));
    }
}

void ConsultWindow::populateConsult(){
    if (*currentConsult == 0){
        qDebug("currentConsult is null");
        return;
    }
    qDebug("in populateConsult");
    QString s;
    (*currentConsult)->print(s);
    qDebug(s.toStdString().c_str());

    QString num;
    ui->date->setDateTime((*currentConsult)->getDate());
    qDebug("set date");
    ui->physID->setText(num.setNum((*currentConsult)->getPhysID()));
    ui->physFName->setText((*currentConsult)->getPhysFirstName());
    qDebug("setPhysfname");
    ui->physLName->setText((*currentConsult)->getPhysLastName());
    qDebug("setphyslname");
    ui->reason->setText((*currentConsult)->getReason());
    qDebug("setreason");
    ui->diagnosis->setText((*currentConsult)->getDiagnosis());
    qDebug("setdiagnosis");

}

void ConsultWindow::clear(){
    ui->date->clear();
    ui->physFName->clear();
    ui->physLName->clear();
    ui->reason->clear();
    ui->diagnosis->clear();
    *currentConsult = 0;
}

void ConsultWindow::on_back_clicked()
{
    control->exitConsultWindow();
}

void ConsultWindow::on_tableWidget_doubleClicked(const QModelIndex &index)
{
    (*currentConsult) = &(*consults)[index.row()];
    populateConsult();
}


void ConsultWindow::on_clear_clicked()
{
    clear();
}

void ConsultWindow::on_newfollowup_clicked()
{
    if ((*currentConsult) == 0){
        QMessageBox::information( this, "new follow-up",
                                  "Please select a consultation then retry",
                                  "OK", 0);
        return;
    }
    qDebug("Consultation # from consultwindow: %d", (*currentConsult)->getConsultID());
    control->openFollowupWindow();
}

void ConsultWindow::populateConsultTable(){
    Consultation *consult;
    QString num;
    ui->tableWidget->setRowCount(consults->size());
    QString doc;
    for (int i = 0; i < consults->size(); i ++){
        consult = &(*consults)[i];
        //Physician date reason diagnosis
       //QTableWidgetItem item(consult->)
         ui->tableWidget->setItem(i,0,new QTableWidgetItem(num.setNum(consult->getPhysID())));
         doc.clear();
         doc.append("Dr. ").append(consult->getPhysFirstName()).append(" ").append(consult->getPhysLastName());
         ui->tableWidget->setItem(i,1,new QTableWidgetItem(doc));
         ui->tableWidget->setItem(i,2,new QTableWidgetItem(consult->getDate().toString()));
         ui->tableWidget->setItem(i,3,new QTableWidgetItem(consult->getReason()));
         ui->tableWidget->setItem(i,4,new QTableWidgetItem(consult->getDiagnosis()));
    }
}


void ConsultWindow::on_getfollowups_clicked()
{
    if ((*currentConsult) == 0){
        QMessageBox::information( this, "get followups",
                                  "Please select a consultation then retry",
                                  "OK", 0);
        return;
    }
    followups->clear();
    errorType t = storage->getFollowupList(**currentConsult, *followups, 0);
    if (t!=OK){
        QMessageBox::information( this, "Follow-up Search",
                                  "Database error. Is the server running?",
                                    "OK",0);
        return;
    }
    qDebug("Followups size %d", followups->size());
    if (followups->size() == 0){
        QMessageBox::information( this, "Follow-up Search",
                                  "No follow-ups found.",
                                    "OK",0);
        return;
    }
    control->openFollowupWindow();
}
