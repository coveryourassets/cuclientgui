#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include "storage/custore.h"
#include "maincontroller.h"

class MainController;

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoginDialog(cuStore * storage, User *user, MainController *main, QWidget *parent = 0);
    ~LoginDialog();
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::LoginDialog *ui;
    cuStore * storage;
    User * user;
    MainController * main;
};

#endif // LOGINDIALOG_H
