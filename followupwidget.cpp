#include "followupwidget.h"
#include "ui_followupwidget.h"

FollowupWidget::FollowupWidget(MainController *control,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FollowupWidget)
{
    ui->setupUi(this);
    this->control = control;
    this->followups = control->getFollowups();
    currentPatient = this->control->getCurrentPatient();
    currentConsult = control->getCurrentConsult();
    currentFollowup = control->getCurrentFollowup();
    storage = control->getStorage();
    //initializeWithNewConsultation();

}

FollowupWidget::~FollowupWidget()
{
    delete ui;
}

void FollowupWidget::initializeWithNewConsultation(){
    QString num;
    qDebug("here at initializer");

    //storage->getFollowupList(*(new Consultation()),*followups);
    //control->getCurrentPatient();
    qDebug("storage ok");
    if ((*currentPatient)==0){
        qDebug("currentPatient is 0");
    }

    qDebug("setPatientID");
    ui->patientID->setText(num.setNum((*currentPatient)->getPatientID()));
    qDebug("setPatientfname") ;
    ui->patientfname->setText((*currentPatient)->getFirstName());
    qDebug("setPatientlname") ;
    ui->patientlname->setText((*currentPatient)->getLastName());
    qDebug("set physID") ;
    ui->physID->setText(num.setNum((*currentConsult)->getPhysID()));
    qDebug("physID: %s",num.toStdString().c_str()) ;
    qDebug("set physfname") ;
    ui->physfname->setText((*currentConsult)->getPhysFirstName());
    qDebug("set physlname") ;
    ui->physlname->setText((*currentConsult)->getPhysLastName());
    qDebug("done initializer") ;


}

void FollowupWidget::on_save_clicked()
{
    QString message;
    QString button;
    bool update = false;
    if ((*currentFollowup)==0){
        message.append("Save new follow-up?");
        button.append("Save");
        (*currentFollowup)= new Followup();
    }else{
        message.append("Update follow-up?");
        button.append("Update");
        update = true;
    }
    if (QMessageBox::information( this, "Follow-up",
                              message,
                              button,"Cancel", 0,1)==1){
        return;
    }
    QString description(ui->description->toPlainText());
    QString results(ui->results->toPlainText());

    qDebug("Consultation # from followupwindow: %d", (*currentConsult)->getConsultID());

    (*currentFollowup)->setConsultID((*currentConsult)->getConsultID());
    (*currentFollowup)->setDate(ui->date->date());
    (*currentFollowup)->setDescription(description);
    (*currentFollowup)->setResults(results);
    (*currentFollowup)->setStatus((Status)(ui->status->currentIndex()));

    errorType t;
    if (update){
        t = storage->updateFollowup(**currentFollowup);
    }else{
        t = storage->saveNewFollowup(**currentFollowup);
    }

    if (t==OK){
        button.append(" succeeded");
    }else{
        button.append(" failed");
    }
    QMessageBox::information( this, "follow-up",
                                      button,
                                          "OK",0);
}



void FollowupWidget::on_tableWidget_clicked(const QModelIndex &index)
{
    (*currentFollowup)=&(*followups)[index.row()];
    populateFollowupForm();
}

//private

void FollowupWidget::populateFollowupForm(){
    if ((*currentFollowup) == 0){
        QMessageBox::warning( this, "Developer error",
                                  "No currentFollowup available.",
                                    "OK",0);
    }
    QString num;
    ui->patientfname->setText((*currentPatient)->getFirstName());
    ui->patientlname->setText((*currentPatient)->getLastName());
    ui->patientID->setText(num.setNum((*currentPatient)->getPatientID()));

    ui->physfname->setText((*currentConsult)->getPhysFirstName());
    ui->physlname->setText((*currentConsult)->getPhysLastName());
    ui->physID->setText(num.setNum((*currentConsult)->getPhysID()));

    ui->description->setText((*currentFollowup)->getDescription());
    ui->results->setText((*currentFollowup)->getResults());

    ui->date->setDate((*currentFollowup)->getDate());

    //enum Status {pending, overdue, results_received, complete};

}

void FollowupWidget::populateFollowupTable(){

}
