#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "storage/custore.h"
#include "maincontroller.h"

class MainController;
class QModelIndex;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(MainController *control, QWidget *parent = 0);
    ~MainWindow();

    //testing
    void swapWidget(QWidget *widget);

private slots:

    void on_newpatient_clicked();

    void on_searchfullname_clicked();

    void on_searchlname_clicked();

    void on_searchfname_clicked();

    void on_searchhcard_clicked();

private:

    Ui::MainWindow *ui;
    cuStore *storage;
    User *user;
    MainController * control;
    QList<Patient> * patients;

    //testing
    QWidget *tempwidget;
};

#endif // MAINWINDOW_H


