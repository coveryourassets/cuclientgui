/********************************************************************************
** Form generated from reading UI file 'patientconfirmsave.ui'
**
** Created: Fri Nov 23 05:39:31 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PATIENTCONFIRMSAVE_H
#define UI_PATIENTCONFIRMSAVE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>

QT_BEGIN_NAMESPACE

class Ui_patientConfirmSave
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label;

    void setupUi(QDialog *patientConfirmSave)
    {
        if (patientConfirmSave->objectName().isEmpty())
            patientConfirmSave->setObjectName(QString::fromUtf8("patientConfirmSave"));
        patientConfirmSave->resize(400, 120);
        buttonBox = new QDialogButtonBox(patientConfirmSave);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(30, 60, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(patientConfirmSave);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 30, 281, 17));

        retranslateUi(patientConfirmSave);
        QObject::connect(buttonBox, SIGNAL(accepted()), patientConfirmSave, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), patientConfirmSave, SLOT(reject()));

        QMetaObject::connectSlotsByName(patientConfirmSave);
    } // setupUi

    void retranslateUi(QDialog *patientConfirmSave)
    {
        patientConfirmSave->setWindowTitle(QApplication::translate("patientConfirmSave", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("patientConfirmSave", "Save new patient?", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class patientConfirmSave: public Ui_patientConfirmSave {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PATIENTCONFIRMSAVE_H
