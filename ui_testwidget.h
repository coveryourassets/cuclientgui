/********************************************************************************
** Form generated from reading UI file 'testwidget.ui'
**
** Created: Wed Nov 21 13:31:29 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTWIDGET_H
#define UI_TESTWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TestWidget
{
public:
    QLabel *label;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QPushButton *searchfname;
    QLineEdit *fname;
    QPushButton *searchlname;
    QLineEdit *lname;
    QPushButton *searchhcard;
    QLineEdit *healthcard;

    void setupUi(QWidget *TestWidget)
    {
        if (TestWidget->objectName().isEmpty())
            TestWidget->setObjectName(QString::fromUtf8("TestWidget"));
        TestWidget->resize(400, 300);
        label = new QLabel(TestWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(130, 130, 111, 17));
        formLayoutWidget = new QWidget(TestWidget);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 0, 341, 121));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        searchfname = new QPushButton(formLayoutWidget);
        searchfname->setObjectName(QString::fromUtf8("searchfname"));

        formLayout->setWidget(1, QFormLayout::LabelRole, searchfname);

        fname = new QLineEdit(formLayoutWidget);
        fname->setObjectName(QString::fromUtf8("fname"));

        formLayout->setWidget(1, QFormLayout::FieldRole, fname);

        searchlname = new QPushButton(formLayoutWidget);
        searchlname->setObjectName(QString::fromUtf8("searchlname"));

        formLayout->setWidget(2, QFormLayout::LabelRole, searchlname);

        lname = new QLineEdit(formLayoutWidget);
        lname->setObjectName(QString::fromUtf8("lname"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lname);

        searchhcard = new QPushButton(formLayoutWidget);
        searchhcard->setObjectName(QString::fromUtf8("searchhcard"));

        formLayout->setWidget(3, QFormLayout::LabelRole, searchhcard);

        healthcard = new QLineEdit(formLayoutWidget);
        healthcard->setObjectName(QString::fromUtf8("healthcard"));

        formLayout->setWidget(3, QFormLayout::FieldRole, healthcard);


        retranslateUi(TestWidget);

        QMetaObject::connectSlotsByName(TestWidget);
    } // setupUi

    void retranslateUi(QWidget *TestWidget)
    {
        TestWidget->setWindowTitle(QApplication::translate("TestWidget", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("TestWidget", "Hello world", 0, QApplication::UnicodeUTF8));
        searchfname->setText(QApplication::translate("TestWidget", "First Name", 0, QApplication::UnicodeUTF8));
        searchlname->setText(QApplication::translate("TestWidget", "Last Name", 0, QApplication::UnicodeUTF8));
        searchhcard->setText(QApplication::translate("TestWidget", "Healthcard", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TestWidget: public Ui_TestWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTWIDGET_H
