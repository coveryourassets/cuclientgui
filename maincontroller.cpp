#include "maincontroller.h"
#include "storage/cuclientsocket.h"

MainController::MainController()
{
    currentPatient = 0;
    currentConsult = 0;
    storage = new cuClientSocket();
    user = new User();
    patients = new QList<Patient>();
    consults = new QList<Consultation>();
    followups = new QList<Followup>();
    mainWindow = new MainWindow(this);
    patwindow = new PatientWindow(this);
    consultwindow = new ConsultWindow(this);
    followupwindow = new FollowupWindow(this);
    followupwidget = followupwindow->getFollowupWidget();
    login = new LoginDialog(storage, user, this);
    login->show(); 
}

MainController::~MainController(){
    delete storage;
    delete user;
    delete patients;
    delete consults;
    delete followups;
    delete mainWindow;
}


//setters
void MainController::setPatients(QList<Patient> *patients){
    this->patients = patients;
}

void MainController::setConsults(QList<Consultation> *consults){
    this->consults = consults;
}

void MainController::setCurrentPatient(Patient* patient){
    currentPatient = patient;
}

void MainController::setCurrentConsult(Consultation* consult){
    currentConsult = consult;
}

//getters
QList<Patient> *MainController::getPatients(){
    return patients;
}

QList<Consultation> *MainController::getConsults(){
    return consults;
}

QList<Followup> *MainController::getFollowups(){
    return followups;
}

Patient ** MainController::getCurrentPatient(){
    return &currentPatient;
}

Consultation **MainController::getCurrentConsult(){
    return &currentConsult;
}

Followup **MainController::getCurrentFollowup(){
    return &currentFollowup;
}

cuStore * MainController::getStorage(){
    return storage;
}

User * MainController::getUser(){
    return user;
}


//etc
void MainController::addPatient(Patient* pat){
    patients->push_back(*pat);
}


void MainController::loginSuccess(){
    qDebug("this get called?");
    login->done(0);
    mainWindow->show();
}


void MainController::newPatient(){
    mainWindow->close();
    patwindow->show();
   // mainWindow->populatePatients(patients);
}

void MainController::exitPatientWindow(){
    //patwindow->clearPatientForm();
    patwindow->close();
    mainWindow->show();
}

void MainController::newConsult(){
   // patwindow->close();
    consultwindow->initializeWithNewPatient();
    consultwindow->show();
}

void MainController::exitConsultWindow(){
    consultwindow->close();
    patwindow->show();
}

void MainController::openPatientWindow(){
    patwindow->populatePatientTable();
    patwindow->show();
}

void MainController::openFollowupWindow(){
    //consultwindow->close();
    qDebug("initializing follow-up widget");
    followupwindow->show();
    QString add;
    Address a = currentPatient->getAddress();
    a.print(add);
    qDebug("Address: %s",add.toStdString().c_str());
    qDebug("all good at main controller");
    followupwidget->initializeWithNewConsultation();
}

void MainController::openConsultWindow(){
    consultwindow->initializeWithNewPatient();
    //consultwindow->show();
}


